$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000
    });

    $('#contacto').on('show.bs.modal', function (e){
        console.log('El modal se está ejecutando');
        $('#contactoBtn').removeClass('btn-outline-primary');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('El modal se está ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('El modal se ocultó');
        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-outline-primary');
        $('#contactoBtn').prop('disabled', false);
    });
})